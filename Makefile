define SHELLSCRIPT
mkdir -p ../build/chrome
if type -a JSsyntaxcheck.sh > /dev/null 2>&1 ; 
then
	JSsyntaxcheck.sh chrome/content/*.js || exit 1; 
fi
#Version modify
cp -f install.rdf install.rdf.orig
sed -i -e "s/<\/em:version>/."`git rev-parse --short HEAD`"<\/em:version>/g" install.rdf
sed -i -e "s/<em:updateURL>.\+<\/em:updateURL>/<em:updateURL>.\+<\/em:updateURL>/g" install.rdf
bash build.sh
cp -f install.rdf install.rdf.bak
cp -f install.rdf.orig install.rdf
git clean -n -x -e "install.rdf.*" -e kancolletimer.xpi
endef
export SHELLSCRIPT
all::
	echo "$${SHELLSCRIPT}" > /tmp/$$$$ ; $(SHELL) /tmp/$$$$ ; rm -f /tmp/$$$$
